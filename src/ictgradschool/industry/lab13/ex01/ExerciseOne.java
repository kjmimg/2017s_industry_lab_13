package ictgradschool.industry.lab13.ex01;

/**
 * Created by jkua423 on 12/12/2017.
 */
public class ExerciseOne {

    public void start() {
        MyRunnable runnable = new MyRunnable();
        Thread t = new Thread(runnable);
        t.start();

        try{
            t.join();
            System.out.println("Finished!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}

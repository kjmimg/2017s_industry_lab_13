package ictgradschool.industry.lab13.ex03;
import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    private double sum = 0;

    public void start() {

        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");
        long numSamples = Long.parseLong(Keyboard.readInput());

        System.out.println("Enter the number of threads to use for approximation:");
        System.out.print("> ");
        long numThreads = Long.parseLong(Keyboard.readInput());

        long numSamplesPerThread = numSamples / numThreads;

        long startTime = System.currentTimeMillis();



        List<Thread> threads = new ArrayList<>();
        for(int i=0;i<numThreads;i++){
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    double est = estimatePI(numSamplesPerThread);
                    //System.out.println(est);
                    sum += est;
                }
            });
            t.start();
            threads.add(t);
        }

        try{
            for (Thread t : threads) {
                t.join();
            }
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        double estimatedPi = sum / numThreads;

        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(estimatedPi - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");

    }



//    /**
//     * Estimates PI using a multi-threaded Monte Carlo method.
//     */
//    @Override
//    protected double estimatePI(long numSamples) {
//        // ThreadLocalRandom is a non-thread-safe version of Random that's designed to be used on a single thread.
//        // Because it lacks thread-safety (ant its associated overheads), it is noticeably faster than Math.random(),
//        // even when used on a single thread.
//        ThreadLocalRandom tlr = ThreadLocalRandom.current();
//
//        long numInsideCircle = 0;
//
//        for (long i = 0; i < numSamples; i++) {
//
//            double x = tlr.nextDouble();
//            double y = tlr.nextDouble();
//
//            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
//                numInsideCircle++;
//            }
//
//        }
//
//        double estimatedPi = 4.0 * (double) numInsideCircle;
//        return  estimatedPi;
//    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
